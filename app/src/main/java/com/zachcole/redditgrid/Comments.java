package com.zachcole.redditgrid;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.media.Image;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;


public class Comments extends ListActivity {

    private static final String TAG = Comments.class.getSimpleName();
    RedditListItems item = new RedditListItems();
    CommentObject[] comments = new CommentObject[300];
    CommentObject[] finalComments;
    int indent = 0;
    int commentDir = 0;
    String sub = "";
    String perm = "";
    int mPos = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comments);

        Intent intent = getIntent();
        String permalink = intent.getExtras().getString("urlstring");
        perm = permalink;
        int listPosition = intent.getExtras().getInt("position");
        mPos = listPosition;


        Callback callback = new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                Log.e(TAG, "onFailure");
                e.printStackTrace();
            }

            @Override
            public void onResponse(Response response) throws IOException {
                try{
                    String jsonBody = response.body().string();
                    Log.v(TAG, jsonBody);
                    if(response.isSuccessful()){
                        // Get the post information for header
                        JSONArray reddit = new JSONArray(jsonBody);
                        JSONObject reddit2 = reddit.getJSONObject(0);
                        JSONObject data = reddit2.getJSONObject("data");
                        JSONArray children = data.getJSONArray("children");
                        JSONObject data2 = children.getJSONObject(0);
                        JSONObject data3 = data2.getJSONObject("data");
                        item.setThumbnail(data3.getString("thumbnail"));
                        item.setTitle(data3.getString("title"));
                        item.setScore(data3.getInt("score"));
                        item.setUrl(data3.getString("url"));
                        item.setSubreddit(data3.getString("subreddit"));
                        item.setId(data3.getString("id"));
                        item.setAuthor(data3.getString("author"));


                        if(data3.getString("likes") == "true"){
                            item.setLikes(1);
                        }else if(data3.getString("likes") == "false"){
                            item.setLikes(-1);
                        }else{
                            item.setLikes(0);
                        }

                        sub = item.getSubreddit();

                        // Get actual comment text
                        JSONObject comm = reddit.getJSONObject(1);
                        JSONObject commdata = comm.getJSONObject("data");
                        JSONArray commchildren = commdata.getJSONArray("children");
                        for(int i = 0; i < commchildren.length() - 1; i++){
                            CommentObject commentObj = new CommentObject();
                            JSONObject tier1 = commchildren.getJSONObject(i);
                            JSONObject tier1data = tier1.getJSONObject("data");
                            if(tier1data.has("author") && tier1data.has("body") && tier1data.has("score")) {
                                commentObj.setBody(tier1data.getString("body"));
                                commentObj.setAuthor(tier1data.getString("author"));
                                commentObj.setScore(tier1data.getInt("score"));
                                commentObj.setPadding(indent);
                                commentObj.setLayer(1);
                                commentObj.setId(tier1data.getString("id"));

                                if(tier1data.getString("likes").equals("true")){
                                    commentObj.setLikes(1);
                                }else if(tier1data.getString("likes").equals("false")){
                                    commentObj.setLikes(-1);
                                }else{
                                    commentObj.setLikes(0);
                                }

                                comments[getIndex()] = commentObj;
                                Log.e(TAG, commentObj.getBody());
                            }

                            if (tier1data.has("replies") && !tier1data.getString("replies").equals("")){
                                JSONObject replies = tier1data.getJSONObject("replies");
                                //JSONObject repliesObj = new JSONObject(blah);
                                getReplies(replies, indent, 1);

                            }

//                            try{
//                                String replies = tier1data.getString("replies");
//                                JSONObject repliesObj = new JSONObject(replies);
//                                getReplies(repliesObj, indent);
//                            }catch (JSONException e){
//                                Log.e(TAG, "JSON Exception 1");
//                                e.printStackTrace();
//                            }
                        }

                        finalComments = new CommentObject[getIndex()];
                        for(int i = 0; i < getIndex(); i++){
                            finalComments[i] = comments[i];
                            Log.e(TAG, Integer.toString(finalComments[i].getLayer()));
                        }




                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                View headerView =  ((LayoutInflater)getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.comment_header, null, false);
                                getListView().addHeaderView(headerView);

                                ImageView commentsThumb = (ImageView)findViewById(R.id.commentsThumb);
                                TextView commentsTitle = (TextView)findViewById(R.id.commentsTitle);
                                TextView commentsScore = (TextView)findViewById(R.id.commentsScore);
                                TextView commentsSub = (TextView)findViewById(R.id.commentsSub);
                                TextView commentsUser = (TextView)findViewById(R.id.commentsUser);
                                LinearLayout commentsLikeLL = (LinearLayout)findViewById(R.id.commentsLikeLL);


                                if(item.getTitle().length() < 150){
                                    commentsTitle.setTextSize(20);
                                }else{
                                    commentsTitle.setTextSize(15);
                                }

                                commentsTitle.setText(item.getTitle());
                                commentsScore.setText(Integer.toString(item.getScore()));
                                commentsSub.setText(item.getSubreddit());
                                commentsUser.setText(item.getAuthor());

                                if (item.getThumbnail() == "default") {
                                    Picasso.with(getBaseContext())
                                            .load("http://www.openprocessing.org/sketch/11740/thumbnail")
                                            .into(commentsThumb);
                                } else {
                                    Picasso.with(getBaseContext())
                                            .load(item.getThumbnail())
                                            .into(commentsThumb);
                                }

                                if(item.getLikes() == 1) {
                                    //convertView.setBackgroundColor(Color.parseColor("#ff5700"));
                                    //commentsLikeLL.setBackgroundColor(Color.parseColor("#ff5700"));
                                    commentsScore.setTextColor(Color.parseColor("#ff5700"));
                                    ((ImageButton)findViewById(R.id.linkUp)).setImageResource(R.drawable.uporange);
                                    ((ImageButton)findViewById(R.id.linkDown)).setImageResource(R.drawable.downwhite);
                                }else if(item.getLikes() == -1){
                                    //convertView.setBackgroundColor(Color.parseColor("#0084d3"));
                                    //commentsLikeLL.setBackgroundColor(Color.parseColor("#0084d3"));
                                    commentsScore.setTextColor(Color.parseColor("#0084d3"));
                                    ((ImageButton)findViewById(R.id.linkUp)).setImageResource(R.drawable.upwhite);
                                    ((ImageButton)findViewById(R.id.linkDown)).setImageResource(R.drawable.downblue);
                                } else{
                                    //convertView.setBackgroundColor(Color.parseColor("#ff282828"));
                                    //commentsLikeLL.setBackgroundColor(Color.parseColor("#ff282828"));
                                    commentsScore.setTextColor(Color.parseColor("#ffffff"));
                                    ((ImageButton)findViewById(R.id.linkUp)).setImageResource(R.drawable.upwhite);
                                    ((ImageButton)findViewById(R.id.linkDown)).setImageResource(R.drawable.downwhite);
                                }




                                CommentsAdapter adapter = new CommentsAdapter(getBaseContext(), finalComments);
                                setListAdapter(adapter);

                            }
                        });

                    }

                } catch (IOException e){
                    Log.e(TAG, "Exception caught: IO");
                } catch (JSONException e){
                    Log.e(TAG, "Exception caught: JSON");
                    e.printStackTrace();
                }
            }
        };

        RedditHTTPClient.sharedInstance().getComments(callback, permalink);

    }

//    @Override
//    public void onBackPressed()
//    {
//        super.onBackPressed();  // optional depending on your needs
//
//        Bundle bundle = new Bundle();
//        bundle.putString("suburl", "/r/" + sub);
//        bundle.putInt("position", mPos);
//
//        Intent intent = new Intent(this, FrontPage.class);
//        intent.putExtras(bundle);
//        startActivity(intent);
//    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_comments, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void startWebViewComments(View v){

        //This method handles clicking on the image in the ListView

        ListView list = getListView();

        Bundle bundle = new Bundle();
        String url = "";
        url = item.getUrl();
        Log.v(TAG, "url: " + url);
        bundle.putString("urlstring", url);

        Intent intent = new Intent(this, WebViewActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    public void getReplies(JSONObject replies, int indent, int mlayer) throws JSONException{
        CommentObject obj = new CommentObject();
        JSONObject repliesData = replies.getJSONObject("data");
        JSONArray repliesChildren = repliesData.getJSONArray("children");

        //for(int i = 0; i < repliesChildren.length() - 1; i++){
        //    Log.e(TAG, "i: " + Integer.toString(i));
            int padding = indent + 10;
            int newLayer = mlayer + 1;
            int dpPadding = dpToPx(padding);
            Log.v(TAG, "Padding: " + Integer.toString(dpPadding));
            JSONObject replyChild = repliesChildren.getJSONObject(0);
            JSONObject repliesData2 = replyChild.getJSONObject("data");
            if(repliesData2.has("author") && repliesData2.has("body") && repliesData2.has("score")){
                obj.setAuthor(repliesData2.getString("author"));
                obj.setBody(repliesData2.getString("body"));
                obj.setScore(repliesData2.getInt("score"));
                obj.setId(repliesData2.getString("id"));
                obj.setLayer(newLayer);
                obj.setPadding(dpPadding);
                comments[getIndex()] = obj;
                Log.v(TAG, obj.getBody());
            }


        if(repliesData2.has("likes")) {
            String likes = repliesData2.getString("likes");

            if (likes.equals("true")) {
                obj.setLikes(1);
            } else if (likes.equals("false")) {
                obj.setLikes(-1);
            } else {
                obj.setLikes(0);
            }
        }

            if (repliesData2.has("replies")){
                String reply = repliesData2.getString("replies");
                if(!reply.equals("")) {
                    JSONObject newReplies = new JSONObject(reply);
                    getReplies(newReplies, (padding + 10), newLayer);
                }
            }

        //}
    }

    public int getIndex(){
        int index = 0;
        for(int i = 0; i < comments.length + 2; i++){
            if(comments[i] == null){
                index = i;
                break;
            }
        }
        return index;

    }

    public void postDownVoteLink(View v){

        String postId = item.getId();
        final int direction = item.getLikes();




        Callback callback = new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {

            }

            @Override
            public void onResponse(Response response) throws IOException {
                Log.v(TAG, "Should post downvote");

                if(response.isSuccessful()){
                    Log.v(TAG, "Successful");
                }else{
                    Log.v(TAG, "Nope");
                    Log.v(TAG, response.message());
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        LinearLayout commentsLikeLL = (LinearLayout)findViewById(R.id.commentsLikeLL);
                        ImageButton linkUp = (ImageButton)findViewById(R.id.linkUp);
                        ImageButton linkDown = (ImageButton)findViewById(R.id.linkDown);
                        TextView commentsScore = (TextView)findViewById(R.id.commentsScore);

                        if(direction == 1 || direction == 0){
                            linkUp.setImageResource(R.drawable.upwhite);
                            linkDown.setImageResource(R.drawable.downblue);
                            commentsScore.setTextColor(Color.parseColor("#0084d3"));
                        }else if(direction == -1){
                            linkUp.setImageResource(R.drawable.upwhite);
                            linkDown.setImageResource(R.drawable.downwhite);
                            commentsScore.setTextColor(Color.parseColor("#ffffff"));
                        }
                    }
                });


            }
        };

        //Don't want to send unnecessary requests
        if(direction == 1 || direction == 0) {
            RedditHTTPClient.sharedInstance().postVote(-1, postId, callback);
        }else if (direction == -1){
            RedditHTTPClient.sharedInstance().postVote(0, postId, callback);
        }

        if(direction == 1 || direction == 0){
            item.setLikes(-1);
        }else if (direction == -1){
            item.setLikes(0);
        }
    }

    public void postUpVoteLink(View v){

        String postId = item.getId();
        final int direction = item.getLikes();

        Callback callback = new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {

            }

            @Override
            public void onResponse(Response response) throws IOException {
                Log.v(TAG, "Should post downvote");

                if(response.isSuccessful()){
                    Log.v(TAG, "Successful");
                }else{
                    Log.v(TAG, "Nope");
                    Log.v(TAG, response.message());
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        LinearLayout commentsLikeLL = (LinearLayout)findViewById(R.id.commentsLikeLL);
                        ImageButton linkUp = (ImageButton)findViewById(R.id.linkUp);
                        ImageButton linkDown = (ImageButton)findViewById(R.id.linkDown);
                        TextView commentsScore = (TextView)findViewById(R.id.commentsScore);

                        if(direction == -1 || direction == 0){
                            linkUp.setImageResource(R.drawable.uporange);
                            linkDown.setImageResource(R.drawable.downwhite);
                            commentsScore.setTextColor(Color.parseColor("#ff5700"));
                        }else if(direction == 1){
                            linkUp.setImageResource(R.drawable.upwhite);
                            linkDown.setImageResource(R.drawable.downwhite);
                            commentsScore.setTextColor(Color.parseColor("#ffffff"));
                        }
                    }
                });


            }
        };

        //Don't want to send unnecessary request
        if(direction == -1 || direction == 0) {
            RedditHTTPClient.sharedInstance().postVote(1, postId, callback);
        }else if(direction == 1){
            RedditHTTPClient.sharedInstance().postVote(0, postId, callback);
        }

        if(direction == -1 || direction == 0) {
            item.setLikes(1);
        }else if(direction == 1){
            item.setLikes(0);
        }
    }

    public void postDownVoteComment(View v){

        String postId = "";

        final ListView list = getListView();
        final int position = list.getPositionForView(v);
        int direction;

        for(int i = 0; i < getIndex(); i++){
            if(i == position - 1){
                postId = finalComments[i].getId();
                direction = finalComments[i].getLikes();
                commentDir = direction;
            }
        }

        Callback callback = new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {

            }

            @Override
            public void onResponse(Response response) throws IOException {
                Log.v(TAG, "Should post downvote");


                if(response.isSuccessful()){
                    Log.v(TAG, "Successful");
                }else{
                    Log.v(TAG, "Nope");
                    Log.v(TAG, response.message());
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        View v = getViewByPosition(position, list);
                        if(commentDir == 1 || commentDir == 0){
                            //v.findViewById(R.id.commentColorLL).setBackgroundColor(Color.parseColor("#ff282828"));
                            ((ImageButton) v.findViewById(R.id.upComment)).setImageResource(R.drawable.upwhite);
                            ((TextView) v.findViewById(R.id.commentScore)).setTextColor(Color.parseColor("#0084d3"));
                            ((ImageButton) v.findViewById(R.id.downComment)).setImageResource(R.drawable.downblue);
                        }else if(commentDir == -1){
                            //v.findViewById(R.id.commentColorLL).setBackgroundColor(Color.parseColor("#0084d3"));
                            ((ImageButton) v.findViewById(R.id.upComment)).setImageResource(R.drawable.upwhite);
                            ((TextView) v.findViewById(R.id.commentScore)).setTextColor(Color.parseColor("#ffffff"));
                            ((ImageButton) v.findViewById(R.id.downComment)).setImageResource(R.drawable.downwhite);
                        }
                    }
                });


            }
        };

        //Don't want to send unnecessary requests
        if(commentDir == 1 || commentDir == 0) {
            RedditHTTPClient.sharedInstance().postVoteComment(-1, postId, callback);
        }else if (commentDir == -1){
            RedditHTTPClient.sharedInstance().postVoteComment(0, postId, callback);
        }

        if(commentDir == 1 || commentDir == 0){
            finalComments[position - 1].setLikes(-1);
        }else if(commentDir == -1){
            finalComments[position - 1].setLikes(0);
        }
    }

    public void postUpVoteComment(View v){

        String postId = "";

        final ListView list = getListView();
        final int position = list.getPositionForView(v);
        int direction;

        for(int i = 0; i < getIndex(); i++){
            if(i == position - 1){
                postId = finalComments[i].getId();
                direction = finalComments[i].getLikes();
                commentDir = direction;
            }
        }

        Callback callback = new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {

            }

            @Override
            public void onResponse(Response response) throws IOException {
                Log.v(TAG, "Should post upvote");

                if(response.isSuccessful()){
                    Log.v(TAG, "Successful");
                    Log.v(TAG, response.body().string());
                }else{
                    Log.v(TAG, "Nope");
                    Log.v(TAG, response.message());
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        View v = getViewByPosition(position, list);
                        if(commentDir == 0 || commentDir == -1){
                            //v.findViewById(R.id.commentColorLL).setBackgroundColor(Color.parseColor("#ff5700"));
                            ((ImageButton) v.findViewById(R.id.upComment)).setImageResource(R.drawable.uporange);
                            ((TextView) v.findViewById(R.id.commentScore)).setTextColor(Color.parseColor("#ff5700"));
                            ((ImageButton) v.findViewById(R.id.downComment)).setImageResource(R.drawable.downwhite);
                        }else if(commentDir == 1){
                            //v.findViewById(R.id.commentColorLL).setBackgroundColor(Color.parseColor("#ff282828"));
                            ((ImageButton) v.findViewById(R.id.upComment)).setImageResource(R.drawable.upwhite);
                            ((TextView) v.findViewById(R.id.commentScore)).setTextColor(Color.parseColor("#ffffff"));
                            ((ImageButton) v.findViewById(R.id.downComment)).setImageResource(R.drawable.downwhite);
                        }
                    }
                });


            }
        };

        //Don't want to send unnecessary request
        if(commentDir == -1 || commentDir == 0) {
            RedditHTTPClient.sharedInstance().postVoteComment(1, postId, callback);
        }else if(commentDir == 1){
            RedditHTTPClient.sharedInstance().postVoteComment(0, postId, callback);
        }

        if(commentDir == -1 || commentDir == 0) {
            finalComments[position - 1].setLikes(1);
        }else if (commentDir == 1){
            finalComments[position - 1].setLikes(0);
        }
    }

//    public String returnIndent(int indent){
//        String indentString = "";
//        for(int i = 0; i < indent; i++){
//            indentString += "\t";
//        }
//
//        return indentString;
//    }

    public void openReplyActivity(View v){

        //Toast.makeText(getBaseContext(), "Hello", Toast.LENGTH_SHORT).show();

        final ListView list = getListView();
        final int position = list.getPositionForView(v);
        String commentText = "";
        String commentAuthor = "";
        int commentScore = 0;
        String commentId = "";

        for(int i = 0; i < getIndex(); i++){
            if(i == position - 1){
                commentText = finalComments[i].getBody();
                commentAuthor = finalComments[i].getAuthor();
                commentScore = finalComments[i].getScore();
                commentId = finalComments[i].getId();
            }
        }

        final String auth = commentAuthor;
        final String bod = commentText;
        final int score = commentScore;
        final String id = commentId;


        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Reply?")
                .setMessage(commentText)
                .setNegativeButton("Reply", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Bundle bundle = new Bundle();
                        bundle.putString("author", auth);
                        bundle.putString("body", bod);
                        bundle.putInt("score", score);
                        bundle.putString("id", id);
                        bundle.putString("permalink", perm);
                        bundle.putInt("position", mPos);
                        Intent intent = new Intent(Comments.this, ReplyActivity.class);
                        intent.putExtras(bundle);
                        startActivity(intent);
                    }
                })
                .setPositiveButton("Cancel", null)
                .show();
    }

    //Method that helps handle the return of proper listview position for voting code
    public View getViewByPosition(int pos, ListView listView) {
        final int firstListItemPosition = listView.getFirstVisiblePosition();
        final int lastListItemPosition = firstListItemPosition + listView.getChildCount() - 1;

        if (pos < firstListItemPosition || pos > lastListItemPosition ) {
            return listView.getAdapter().getView(pos, null, listView);
        } else {
            final int childIndex = pos - firstListItemPosition;
            return listView.getChildAt(childIndex);
        }
    }

    public static int pxToDp(int px)
    {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }

    public static int dpToPx(int dp)
    {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }
}
