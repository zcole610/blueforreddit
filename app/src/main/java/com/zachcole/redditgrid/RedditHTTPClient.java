package com.zachcole.redditgrid;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Credentials;
import android.preference.PreferenceManager;
import android.util.Base64;
import android.util.Log;

import com.squareup.okhttp.Authenticator;
import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.Proxy;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.Proxy;
import java.security.SecureRandom;

import okio.Buffer;

/**
 * Created by Zach Cole on 6/3/2015.
 */
public class RedditHTTPClient {

    private static final String AUTHORIZATION_HEADER = "Authorization";
    private static String AUTHORIZATION_VALUE = "";
    private static String AUTHORIZATION_PASS = "";

    private static final String BASE_URL = "https://www.reddit.com";
    private static final String API_USERS = "/api/v1/authorize.compact?";
    private static final String ACCESS_TOKEN = "/api/v1/access_token";
    private static final String CLIENT_ID = "PZuQlf9CIQt6lg";
    private static final String TYPE = "code";
    SessionIdentifierGenerator rand = new SessionIdentifierGenerator();
    private String RANDOM_STRING = rand.nextSessionId();
    private static final String URI = "http://zachcole.github.io/redditgrid/";
    private static final String VOTE_URL = "/api/vote";

    private static final String OAUTH_URL = "https://oauth.reddit.com";
    String token;
    String refresh_token;
    private static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    private static final MediaType URL_ENCODED = MediaType.parse("application/x-www-form-urlencoded");

    String AUTHORIZATION_VALUE_64 = getBase64(CLIENT_ID + ":" + AUTHORIZATION_PASS);





    private static final String TAG = RedditHTTPClient.class.toString();

    //Instance Variables
    private static RedditHTTPClient sInstance = null;

    private OkHttpClient client;

    //Constructors
    protected RedditHTTPClient(){
        client = new OkHttpClient();
    }

    public static synchronized RedditHTTPClient sharedInstance(){
        if(sInstance == null){
            sInstance = new RedditHTTPClient();
        }

        return sInstance;
    }


    //Authentication

    public String AuthorizationURL (){
        return BASE_URL + API_USERS + "client_id=" + CLIENT_ID + "&response_type=" + TYPE + "&state=" + RANDOM_STRING + "&redirect_uri=" + URI + "&duration=permanent" + "&scope=identity,vote,read,mysubreddits,submit";
    }

    public void postAccessToken(Callback callback, String code){
        Request request = new Request.Builder()
                .header(AUTHORIZATION_HEADER,"Basic " + getBase64(CLIENT_ID + ":" + AUTHORIZATION_PASS))
                .post(RequestBody.create(URL_ENCODED, "grant_type=authorization_code&code=" + code + "&redirect_uri=" + URI))
                .url(BASE_URL + ACCESS_TOKEN)
                .build();

        Log.v(TAG, AUTHORIZATION_VALUE_64);

        Call call = client.newCall(request);
        call.enqueue(callback);

    }


    public void refreshToken(Callback callback){
        Request request = new Request.Builder()
                .header(AUTHORIZATION_HEADER, "Basic " + getBase64(CLIENT_ID + ":" + AUTHORIZATION_PASS))
                .post(RequestBody.create(URL_ENCODED, "grant_type=refresh_token&refresh_token=" + refresh_token))
                .url(BASE_URL + ACCESS_TOKEN)
                .build();

        System.out.println(refresh_token);
        System.out.println(token);

        Call call = client.newCall(request);
        call.enqueue(callback);

    }


    //Front Page

    public void getFrontPage(Callback callback, String url){
        Request request = new Request.Builder()
                .header(AUTHORIZATION_HEADER, AUTHORIZATION_VALUE)
                .url(OAUTH_URL + url +  "hot.json")
                .build();

        Call call = client.newCall(request);
        call.enqueue(callback);
    }

    public void getFrontPageMore(Callback callback, String id, int count, int limit, String url){
        Request request = new Request.Builder()
                .header(AUTHORIZATION_HEADER, AUTHORIZATION_VALUE)
                .url(OAUTH_URL + url +  "hot?after=" + id + "&count=" + count + "&limit=" + limit + ".json")
                .build();

        Call call = client.newCall(request);
        call.enqueue(callback);
    }

    //Comments

    public void getComments(Callback callback, String permalink){

        Request request = new Request.Builder()
                .header(AUTHORIZATION_HEADER, AUTHORIZATION_VALUE)
                .url(OAUTH_URL + permalink + ".json")
                .build();

        Call call = client.newCall(request);
        call.enqueue(callback);
    }

    //User

    public void getUser(Callback callback){
        Request request = new Request.Builder()
                .header(AUTHORIZATION_HEADER, AUTHORIZATION_VALUE)
                .url(OAUTH_URL + "/api/v1/me.json")
                .build();

        Call call = client.newCall(request);
        call.enqueue(callback);
    }

    public final class SessionIdentifierGenerator{
        private SecureRandom random = new SecureRandom();

        public String nextSessionId(){
            return new BigInteger(130, random).toString(32);
        }

    }

    public void setTokens(Context context){
        SharedPreferences prefs = context.getSharedPreferences("com.zachcole.redditgrid", Context.MODE_PRIVATE);
        //token = PreferenceManager.getDefaultSharedPreferences(context).getString("com.zachcole.redditgrid.token","DEFAULT");
        //refresh_token = PreferenceManager.getDefaultSharedPreferences(context).getString("com.zachcole.redditgrid.refresh_token","DEFAULT");
        token = prefs.getString("com.zachcole.redditgrid.token", "");
        refresh_token = prefs.getString("com.zachcole.redditgrid.refresh_token", "");
        AUTHORIZATION_VALUE = "bearer " + token;
    }

    //Voting methods
    public void postVote(int direction, String id, Callback callback){

        Request request = new Request.Builder()
                .header(AUTHORIZATION_HEADER, AUTHORIZATION_VALUE)
                .url(OAUTH_URL + VOTE_URL)
                .post(RequestBody.create(URL_ENCODED, "dir=" + Integer.toString(direction) + "&id=t3_" + id + "&uh=null"))
                .build();

        Log.v(TAG, bodyToString(request));

        client.newCall(request).enqueue(callback);
    }

    public void postVoteComment(int direction, String id, Callback callback){

        Request request = new Request.Builder()
                .header(AUTHORIZATION_HEADER, AUTHORIZATION_VALUE)
                .url(OAUTH_URL + VOTE_URL)
                .post(RequestBody.create(URL_ENCODED, "dir=" + Integer.toString(direction) + "&id=t1_" + id + "&uh=null"))
                .build();

        Log.v(TAG, bodyToString(request));

        client.newCall(request).enqueue(callback);
    }

    private static String bodyToString(final Request request){

        try {
            final Request copy = request.newBuilder().build();
            final Buffer buffer = new Buffer();
            copy.body().writeTo(buffer);
            return buffer.readUtf8();
        } catch (final IOException e) {
            return "did not work";
        }
    }

    //Subreddit defaults
    public void getSubs(Callback callback) {
        Request request = new Request.Builder()
                .header(AUTHORIZATION_HEADER, AUTHORIZATION_VALUE)
                .url(OAUTH_URL + "/subreddits/popular?limit=8.json")
                .build();

        client.newCall(request).enqueue(callback);
    }

    //Search subs
    public void searchSubs(Callback callback, String search){
        Request request = new Request.Builder()
                .header(AUTHORIZATION_HEADER, AUTHORIZATION_VALUE)
                .url(OAUTH_URL + "/subreddits/search.json?q=" + search)
                .build();

        client.newCall(request).enqueue(callback);
    }

    //Post comment
    public void postComment(Callback callback, String body, String id){
        Request request = new Request.Builder()
                .header(AUTHORIZATION_HEADER, AUTHORIZATION_VALUE)
                .url(OAUTH_URL + "/api/comment")
                .post(RequestBody.create(URL_ENCODED, "api_type=json&text=" + body + "&thing_id=t1_" + id))
                .build();

        client.newCall(request).enqueue(callback);
    }

    public String getBase64(String string){
        byte[] data = null;
        try {
            data = string.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        }
        String base64 = Base64.encodeToString(data, Base64.DEFAULT);
        return base64;
    }


}
