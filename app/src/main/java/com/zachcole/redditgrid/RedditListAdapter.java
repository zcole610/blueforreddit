package com.zachcole.redditgrid;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.Image;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Zach Cole on 6/4/2015.
 */
public class RedditListAdapter extends BaseAdapter {

    private Context mContext;
    private RedditListItems[] mRedditListItems;

    public RedditListAdapter(Context context, RedditListItems[] redditListItems){
        mContext = context;
        mRedditListItems = redditListItems;

    }

    @Override
    public int getCount() {
        return mRedditListItems.length;
    }

    @Override
    public Object getItem(int position) {
        return mRedditListItems[position];
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;


        if(convertView == null){
            //brand new
            convertView = LayoutInflater.from(mContext).inflate(R.layout.reddit_list_item, null);
            holder = new ViewHolder();
            holder.thumbnailImageView = (ImageView)convertView.findViewById(R.id.thumbnailImageView);
            holder.scoreTextView = (TextView)convertView.findViewById(R.id.scoreTextView);
            holder.titleTextView = (TextView)convertView.findViewById(R.id.titleTextView);
            holder.subredditTextView = (TextView)convertView.findViewById(R.id.subredditTextView);
            holder.linearLayout1 = (LinearLayout)convertView.findViewById(R.id.linearLayout1);
            holder.linearLayout2 = (LinearLayout)convertView.findViewById(R.id.linearLayout2);
            holder.commentsImageBtn = (ImageButton)convertView.findViewById(R.id.commentsImageBtn);
            holder.authorTextView = (TextView)convertView.findViewById(R.id.authorTextView);
            holder.upvoteImageBtn = (ImageButton)convertView.findViewById(R.id.upvoteImageBtn);
            holder.downvoteImageBtn = (ImageButton)convertView.findViewById(R.id.downvoteImageBtn);

            convertView.setTag(holder);
        }else{
            holder = (ViewHolder)convertView.getTag();
        }

        RedditListItems rItems = mRedditListItems[position];

        if(rItems.getTitle().length() >=150){
            holder.titleTextView.setTextSize(15);
        }else{
            holder.titleTextView.setTextSize(20);
        }

        //holder.thumbnailImageView.setImageBitmap();
        holder.scoreTextView.setText(rItems.getScore() + "");
        holder.titleTextView.setText(rItems.getTitle() + "");
        holder.subredditTextView.setText(rItems.getSubreddit() + "");
        holder.authorTextView.setText(rItems.getAuthor());

        String url = rItems.getThumbnail();

        if(rItems.getThumbnail().equals("default") || rItems.getThumbnail().equals("") || rItems.getThumbnail().equals("self")) {
            holder.thumbnailImageView.setImageResource(R.drawable.no_thumb);
        }else if(rItems.getThumbnail().equals("nsfw")){
            holder.thumbnailImageView.setImageResource(R.drawable.nsfw);
        }else {
            Picasso.with(mContext)
                    .load(rItems.getThumbnail())
                    .into(holder.thumbnailImageView);
        }


        if(rItems.getLikes() == 1) {
            holder.upvoteImageBtn.setImageResource(R.drawable.uporange);
            holder.scoreTextView.setTextColor(Color.parseColor("#ff5700"));
            holder.downvoteImageBtn.setImageResource(R.drawable.downwhite);
        }else if(rItems.getLikes() == -1){
            holder.upvoteImageBtn.setImageResource(R.drawable.upwhite);
            holder.scoreTextView.setTextColor(Color.parseColor("#0084d3"));
            holder.downvoteImageBtn.setImageResource(R.drawable.downblue);
        } else{
            holder.upvoteImageBtn.setImageResource(R.drawable.upwhite);
            holder.scoreTextView.setTextColor(Color.parseColor("#ffffff"));
            holder.downvoteImageBtn.setImageResource(R.drawable.downwhite);
        }


        return convertView;
    }

    private static class ViewHolder{
        ImageView thumbnailImageView;
        TextView titleTextView;
        TextView scoreTextView;
        TextView subredditTextView;
        LinearLayout linearLayout1;
        LinearLayout linearLayout2;
        ImageButton commentsImageBtn;
        ImageButton downvoteImageBtn;
        ImageButton upvoteImageBtn;
        TextView authorTextView;
    }



}
