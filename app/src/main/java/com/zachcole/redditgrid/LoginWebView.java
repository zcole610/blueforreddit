package com.zachcole.redditgrid;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;


public class LoginWebView extends Activity {

    private int mInterval = 5000;
    private Handler mHandler;

    private static final String TAG = LoginWebView.class.getSimpleName();

    private WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_web_view);

        Intent intent = getIntent();
        String url = intent.getExtras().getString("urlstring");
        Log.v(TAG, url);

        webView = (WebView)findViewById(R.id.webViewLogin);
        webView.setWebViewClient(new WebViewClient());
        //webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(url);

        mHandler = new Handler();

        startRepeatingTask();

//        final Handler handler = new Handler();
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                String token = webView.getUrl().substring(webView.getUrl().indexOf("=") + 1, webView.getUrl().indexOf("&"));
//                RedditHTTPClient.sharedInstance().setToken(token);
//                Log.v(TAG, "CURRENT URL: " + webView.getUrl());
//                Log.v(TAG, "CURRENT URL: " + token);
//
//                Intent intent = new Intent(getBaseContext(), MainActivity.class);
//                startActivity(intent);
//            }
//        }, 10000);



    }

    @Override
    public void onStop(){
        super.onStop();
        mHandler.removeCallbacks(mStatusChecker);
    }

    Runnable mStatusChecker = new Runnable() {
        @Override
        public void run() {

            Log.v(TAG, webView.getUrl());

            if(webView.getUrl().substring(0,37).equals("http://zachcole.github.io/redditgrid/")){
                Log.v(TAG, "Inside the if statement");
                String code = webView.getUrl().substring(webView.getUrl().indexOf("&") + 6);
                Log.v(TAG, code);

                RedditHTTPClient.sharedInstance().postAccessToken(callbackToken, code);
//                RedditHTTPClient.sharedInstance().setTokens(getBaseContext());
//                RedditHTTPClient.sharedInstance().getUser(userCallback);
//                RedditHTTPClient.sharedInstance().getSubs(callback);


            }else {
                mHandler.postDelayed(mStatusChecker, mInterval);
            }
        }
    };

    void startRepeatingTask(){
            mStatusChecker.run();
    }

//    void runCallbacks(){
//        //get the code
//        String code = webView.getUrl().substring(webView.getUrl().indexOf("&") + 6);
//        Log.v(TAG, code);
//
//        RedditHTTPClient.sharedInstance().postAccessToken(callbackToken, code);
//
////                //Using SharedPreferences allows to save the token to the device
////                SharedPreferences prefs = getBaseContext().getSharedPreferences("com.zachcole.redditgrid", MODE_PRIVATE);
////                SharedPreferences.Editor edit = prefs.edit();
////
////                edit.clear();
////                //edit.putString("com.zachcole.redditgrid.token", token);
////                edit.commit();
//
//        RedditHTTPClient.sharedInstance().setTokens(getBaseContext());
//        RedditHTTPClient.sharedInstance().getUser(userCallback);
//        RedditHTTPClient.sharedInstance().getSubs(callback);
//    }

    //this callback saves the top8 subs to SharedPreferences
    Callback callback = new Callback() {
        @Override
        public void onFailure(Request request, IOException e) {

        }

        @Override
        public void onResponse(Response response) throws IOException {
            try {
                Log.e(TAG, "Working? Part 3");
                String jsonBody = response.body().string();
                String[] subs = new String[8];
                if(response.isSuccessful()) {
                    //Log.v(TAG, jsonBody);
                    JSONObject json = new JSONObject(jsonBody);
                    JSONObject data = json.getJSONObject("data");
                    JSONArray children = data.getJSONArray("children");

                    for(int i = 0; i < 8; i++){
                        JSONObject child = children.getJSONObject(i);
                        JSONObject childData = child.getJSONObject("data");
                        subs[i] = childData.getString("url");
                        Log.v(TAG, "url: " + subs[i]);
                    }


                    SharedPreferences prefs = getApplicationContext().getSharedPreferences("com.zachcole.redditgrid", MODE_PRIVATE);
                    SharedPreferences.Editor edit = prefs.edit();

                    for(int i = 0; i < 8; i++) {
                        edit.putString("com.zachcole.redditgrid.sub" + Integer.toString(i), subs[i]).apply();
                    }

                    Intent intent = new Intent(getBaseContext(), MainActivity.class);
                    startActivity(intent);
                    finish();
                    Login.a.finish();

                }

            } catch (IOException e){
                Log.e(TAG, "Exception caught: IO");
                e.printStackTrace();
            } catch (JSONException e){
                Log.e(TAG, "Exception caught: JSON");
                e.printStackTrace();
            }
        }
    };


    Callback callbackToken = new Callback() {
        @Override
        public void onFailure(Request request, IOException e) {

        }

        @Override
        public void onResponse(Response response) throws IOException {
            try {
                String jsonBody = response.body().string();
                Log.v(TAG, jsonBody);
                JSONObject json = new JSONObject(jsonBody);
                String token = json.getString("access_token");
                String refToken = json.getString("refresh_token");
                long time = System.currentTimeMillis();

                //Using SharedPreferences allows to save the token to the device
                //PreferenceManager.getDefaultSharedPreferences(getBaseContext()).edit().putString("com.zachcole.redditgrid.token", token).commit();
                //PreferenceManager.getDefaultSharedPreferences(getBaseContext()).edit().putString("com.zachcole.redditgrid.refresh_token", refToken).commit();
                //PreferenceManager.getDefaultSharedPreferences(getBaseContext()).edit().putLong("com.zachcole.redditgrid.time", time).commit();
                SharedPreferences prefs = getApplicationContext().getSharedPreferences("com.zachcole.redditgrid", MODE_PRIVATE);
                SharedPreferences.Editor edit = prefs.edit();

                edit.putString("com.zachcole.redditgrid.token", token);
                edit.putString("com.zachcole.redditgrid.refresh_token", refToken);
                edit.putLong("com.zachcole.redditgrid.time", time);
                edit.apply();

                Log.e(TAG, "Working?");

                RedditHTTPClient.sharedInstance().setTokens(getApplicationContext());
                RedditHTTPClient.sharedInstance().getUser(userCallback);

            }catch (JSONException e){
                Log.e(TAG, "JSONException:");
                e.printStackTrace();
            }

        }
    };

    Callback userCallback = new Callback() {
        @Override
        public void onFailure(Request request, IOException e) {

        }

        @Override
        public void onResponse(Response response) throws IOException {

            try {
                Log.e(TAG, "Working? Part 2");
                String jsonBody = response.body().string();
                Log.e(TAG, jsonBody);
                if(response.isSuccessful()) {
                    Log.v(TAG, jsonBody);
                    String user = "";
                    JSONObject body = new JSONObject(jsonBody);
                    user = body.getString("name");

                    SharedPreferences prefs = getApplicationContext().getSharedPreferences("com.zachcole.redditgrid", MODE_PRIVATE);
                    SharedPreferences.Editor edit = prefs.edit();


                    edit.clear();
                    edit.putString("com.zachcole.redditgrid.username", user);
                    edit.apply();

                    RedditHTTPClient.sharedInstance().getSubs(callback);

                }

            } catch (IOException e){
                Log.e(TAG, "Exception caught: ");
            } catch (JSONException e){
                Log.e(TAG, "Exception caught: ");
            }
        }
    };



}
