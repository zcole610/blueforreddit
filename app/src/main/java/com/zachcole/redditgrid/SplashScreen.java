package com.zachcole.redditgrid;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;


public class SplashScreen extends Activity {

    private final int SPLASH_DISPLAY_LENGTH = 1000;
    private final String TAG = SplashScreen.class.getSimpleName();
    Callback callback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        //final SharedPreferences prefs = getBaseContext().getSharedPreferences("com.zachcole.redditgrid", Context.MODE_PRIVATE);
        //final String token = prefs.getString("com.zachcole.redditgrid.token", null);

        setTitle(R.string.app_name);



        /* New Handler to start the Menu-Activity
         * and close this Splash-Screen after some seconds.*/
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //If token is still present, skip login screen
                SharedPreferences prefs = getApplicationContext().getSharedPreferences("com.zachcole.redditgrid", MODE_PRIVATE);
                String token = prefs.getString("com.zachcole.redditgrid.token", null);
                if(token != null){
                    long time = prefs.getLong("com.zachcole.reddigrid.time", 0);
                    if((System.currentTimeMillis() - time) >= 3600){
                        RedditHTTPClient.sharedInstance().refreshToken(callback);
                    }
                    RedditHTTPClient.sharedInstance().setTokens(getBaseContext());
                    Intent intent = new Intent(SplashScreen.this, MainActivity.class);
                    startActivity(intent);
                    SplashScreen.this.finish();

                }else{
                    Intent intent = new Intent(SplashScreen.this, Login.class);
                    startActivity(intent);
                    SplashScreen.this.finish();
                }
            }
        }, SPLASH_DISPLAY_LENGTH);

        callback = new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {

            }

            @Override
            public void onResponse(Response response) throws IOException {
                Log.v(TAG, response.body().string());
                try {
                    if(response.isSuccessful()) {
                        String jsonBody = response.body().string();
                        Log.e(TAG, jsonBody);
                        JSONObject json = new JSONObject(jsonBody);
                        String newToken = json.getString("access_token");
                        Long time = System.currentTimeMillis();
                        SharedPreferences prefs = getApplicationContext().getSharedPreferences("com.zachcole.redditgrid", MODE_PRIVATE);
                        SharedPreferences.Editor edit = prefs.edit();


                        edit.clear();
                        edit.putString("com.zachcole.redditgrid.token", newToken);
                        edit.putLong("com.zachcole.redditgrid.time", time);
                        edit.apply();
                    }
                }catch (JSONException e){
                    Log.e(TAG, "JSONException");
                    e.printStackTrace();
                }

            }
        };
    }

}
