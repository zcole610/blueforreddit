package com.zachcole.redditgrid;

/**
 * Created by Zach Cole on 6/3/2015.
 */
public class RedditListItems {

    private int mScore;
    private String mThumbnail;
    private String mTitle;
    private String mUrl;
    private int mOrder;
    private String mCommentUrl;
    private int mLikes;
    private String mSubreddit;
    private String mId;
    private String mAuthor;


    public int getScore() {
        return mScore;
    }

    public void setScore(int score) {
        mScore = score;
    }

    public String getThumbnail() {
        return mThumbnail;
    }

    public void setThumbnail(String thumbnail) {
        if(thumbnail.isEmpty()){
            mThumbnail = "default";
        }else{
            mThumbnail = thumbnail;
        }

    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String url) {
        mUrl = url;
    }

    public int getOrder() {
        return mOrder;
    }

    public void setOrder(int order) {
        mOrder = order;
    }

    public String getCommentUrl() {
        return mCommentUrl;
    }

    public void setCommentUrl(String commentUrl) {
        mCommentUrl = commentUrl;
    }

    public int getLikes() {
        return mLikes;
    }

    public void setLikes(int likes) {
        mLikes = likes;
    }

    public String getSubreddit() {
        return mSubreddit;
    }

    public void setSubreddit(String subreddit) {
        mSubreddit = subreddit;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getAuthor() {
        return mAuthor;
    }

    public void setAuthor(String author) {
        mAuthor = author;
    }
}
