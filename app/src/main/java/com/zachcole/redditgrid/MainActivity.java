package com.zachcole.redditgrid;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.OnLongClick;
import butterknife.Optional;


public class MainActivity extends Activity {

    private static final String TAG = MainActivity.class.getSimpleName();
    UserObject mUserObject = new UserObject();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.inject(this);

        final TextView textUsername = (TextView)findViewById(R.id.text_username);
        final TextView sub1 = (TextView)findViewById(R.id.sub1);
        final TextView sub2 = (TextView)findViewById(R.id.sub2);
        final TextView sub3 = (TextView)findViewById(R.id.sub3);
        final TextView sub4 = (TextView)findViewById(R.id.sub4);
        final TextView sub5 = (TextView)findViewById(R.id.sub5);
        final TextView sub6 = (TextView)findViewById(R.id.sub6);
        final TextView sub7 = (TextView)findViewById(R.id.sub7);
        final TextView sub8 = (TextView)findViewById(R.id.sub8);

        SharedPreferences prefs = getApplicationContext().getSharedPreferences("com.zachcole.redditgrid", Context.MODE_PRIVATE);

        sub1.setText(prefs.getString("com.zachcole.redditgrid.sub0", ""));
        sub2.setText(prefs.getString("com.zachcole.redditgrid.sub1", ""));
        sub3.setText(prefs.getString("com.zachcole.redditgrid.sub2", ""));
        sub4.setText(prefs.getString("com.zachcole.redditgrid.sub3", ""));
        sub5.setText(prefs.getString("com.zachcole.redditgrid.sub4", ""));
        sub6.setText(prefs.getString("com.zachcole.redditgrid.sub5", ""));
        sub7.setText(prefs.getString("com.zachcole.redditgrid.sub6", ""));
        sub8.setText(prefs.getString("com.zachcole.redditgrid.sub7", ""));
        textUsername.setText(prefs.getString("com.zachcole.redditgrid.username", ""));

//        Callback callback = new Callback() {
//            @Override
//            public void onFailure(Request request, IOException e) {
//
//            }
//
//            @Override
//            public void onResponse(Response response) throws IOException {
//
//                try {
//                    String jsonBody = response.body().string();
//                    if(response.isSuccessful()) {
//                        Log.v(TAG, jsonBody);
//                        JSONObject body = new JSONObject(jsonBody);
//                        UserObject user = new UserObject();
//                        user.setUserName(body.getString("name"));
//                        mUserObject.setUserName(user.getUserName());
//
//                    }
//
//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            textUsername.setText(mUserObject.getUserName());
//
//
//
//
//                        }
//                    });
//
//                } catch (IOException e){
//                    Log.e(TAG, "Exception caught: ");
//                } catch (JSONException e){
//                    Log.e(TAG, "Exception caught: ");
//                }
//            }
//        };
//
//
//        RedditHTTPClient.sharedInstance().getUser(callback);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_log_out:
                // Call logout method
                logOut();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @OnClick (R.id.signOutBtn)
    public void signOut(View view){
        logOut();
    }


    @OnClick (R.id.frontpageImageButton)
    public void startFrontPage(View view){
        startListView("/");
    }

    @OnClick(R.id.sub1btn)
    public void sub1(){
        SharedPreferences prefs = getApplicationContext().getSharedPreferences("com.zachcole.redditgrid", Context.MODE_PRIVATE);
        startListView(prefs.getString("com.zachcole.redditgrid.sub0", ""));
    }

    @OnLongClick(R.id.sub1btn)
    public boolean sub1long(){
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(getApplicationContext().getSharedPreferences("com.zachcole.redditgrid", Context.MODE_PRIVATE).getString("com.zachcole.redditgrid.sub0", ""))
                .setMessage("Would you like to change the subreddit for slot #1?")
                .setNegativeButton("Change", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Bundle bundle = new Bundle();
                        bundle.putString("subnumber", Integer.toString(1));
                        bundle.putInt("subInt", 0);
                        Intent intent = new Intent(MainActivity.this, ChooseSubActivity.class);
                        intent.putExtras(bundle);
                        startActivity(intent);
                    }
                })
                .setPositiveButton("Cancel", null)
                .show();
        return true;
    }

    @OnClick(R.id.sub2btn)
    public void sub2(){
        SharedPreferences prefs = getApplicationContext().getSharedPreferences("com.zachcole.redditgrid", Context.MODE_PRIVATE);
        startListView(prefs.getString("com.zachcole.redditgrid.sub1", ""));
    }

    @OnLongClick(R.id.sub2btn)
    public boolean sub2long(){
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(getApplicationContext().getSharedPreferences("com.zachcole.redditgrid", Context.MODE_PRIVATE).getString("com.zachcole.redditgrid.sub1", ""))
                .setMessage("Would you like to change the subreddit for slot #2?")
                .setNegativeButton("Change", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Bundle bundle = new Bundle();
                        bundle.putString("subnumber", Integer.toString(2));
                        bundle.putInt("subInt", 1);
                        Intent intent = new Intent(MainActivity.this, ChooseSubActivity.class);
                        intent.putExtras(bundle);
                        startActivity(intent);
                    }
                })
                .setPositiveButton("Cancel", null)
                .show();
        return true;
    }

    @OnClick(R.id.sub3btn)
    public void sub3(){
        SharedPreferences prefs = getApplicationContext().getSharedPreferences("com.zachcole.redditgrid", Context.MODE_PRIVATE);
        startListView(prefs.getString("com.zachcole.redditgrid.sub2", ""));
    }

    @OnLongClick(R.id.sub3btn)
    public boolean sub3long(){
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(getApplicationContext().getSharedPreferences("com.zachcole.redditgrid", Context.MODE_PRIVATE).getString("com.zachcole.redditgrid.sub2", ""))
                .setMessage("Would you like to change the subreddit for slot #3?")
                .setNegativeButton("Change", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Bundle bundle = new Bundle();
                        bundle.putString("subnumber", Integer.toString(3));
                        bundle.putInt("subInt", 2);
                        Intent intent = new Intent(MainActivity.this, ChooseSubActivity.class);
                        intent.putExtras(bundle);
                        startActivity(intent);
                    }
                })
                .setPositiveButton("Cancel", null)
                .show();
        return true;
    }

    @OnClick(R.id.sub4btn)
    public void sub4(){
        SharedPreferences prefs = getApplicationContext().getSharedPreferences("com.zachcole.redditgrid", Context.MODE_PRIVATE);
        startListView(prefs.getString("com.zachcole.redditgrid.sub3", ""));
    }

    @OnLongClick(R.id.sub4btn)
    public boolean sub4long(){
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(getApplicationContext().getSharedPreferences("com.zachcole.redditgrid", Context.MODE_PRIVATE).getString("com.zachcole.redditgrid.sub3", ""))
                .setMessage("Would you like to change the subreddit for slot #4?")
                .setNegativeButton("Change", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Bundle bundle = new Bundle();
                        bundle.putString("subnumber", Integer.toString(4));
                        bundle.putInt("subInt", 3);
                        Intent intent = new Intent(MainActivity.this, ChooseSubActivity.class);
                        intent.putExtras(bundle);
                        startActivity(intent);
                    }
                })
                .setPositiveButton("Cancel", null)
                .show();
        return true;
    }

    @OnClick(R.id.sub5btn)
    public void sub5(){
        SharedPreferences prefs = getApplicationContext().getSharedPreferences("com.zachcole.redditgrid", Context.MODE_PRIVATE);
        startListView(prefs.getString("com.zachcole.redditgrid.sub4", ""));
    }

    @OnLongClick(R.id.sub5btn)
    public boolean sub5long(){
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(getApplicationContext().getSharedPreferences("com.zachcole.redditgrid", Context.MODE_PRIVATE).getString("com.zachcole.redditgrid.sub4", ""))
                .setMessage("Would you like to change the subreddit for slot #5?")
                .setNegativeButton("Change", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Bundle bundle = new Bundle();
                        bundle.putString("subnumber", Integer.toString(5));
                        bundle.putInt("subInt", 4);
                        Intent intent = new Intent(MainActivity.this, ChooseSubActivity.class);
                        intent.putExtras(bundle);
                        startActivity(intent);
                    }
                })
                .setPositiveButton("Cancel", null)
                .show();
        return true;
    }

    @OnClick(R.id.sub6btn)
    public void sub6(){
        SharedPreferences prefs = getApplicationContext().getSharedPreferences("com.zachcole.redditgrid", Context.MODE_PRIVATE);
        startListView(prefs.getString("com.zachcole.redditgrid.sub5", ""));
    }

    @OnLongClick(R.id.sub6btn)
    public boolean sub16ong(){
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(getApplicationContext().getSharedPreferences("com.zachcole.redditgrid", Context.MODE_PRIVATE).getString("com.zachcole.redditgrid.sub5", ""))
                .setMessage("Would you like to change the subreddit for slot #6?")
                .setNegativeButton("Change", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Bundle bundle = new Bundle();
                        bundle.putString("subnumber", Integer.toString(6));
                        bundle.putInt("subInt", 5);
                        Intent intent = new Intent(MainActivity.this, ChooseSubActivity.class);
                        intent.putExtras(bundle);
                        startActivity(intent);
                    }
                })
                .setPositiveButton("Cancel", null)
                .show();
        return true;
    }

    @OnClick(R.id.sub7btn)
    public void sub7(){
        SharedPreferences prefs = getApplicationContext().getSharedPreferences("com.zachcole.redditgrid", Context.MODE_PRIVATE);
        startListView(prefs.getString("com.zachcole.redditgrid.sub6", ""));
    }

    @OnLongClick(R.id.sub7btn)
    public boolean sub7long(){
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(getApplicationContext().getSharedPreferences("com.zachcole.redditgrid", Context.MODE_PRIVATE).getString("com.zachcole.redditgrid.sub6", ""))
                .setMessage("Would you like to change the subreddit for slot #7?")
                .setNegativeButton("Change", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Bundle bundle = new Bundle();
                        bundle.putString("subnumber", Integer.toString(7));
                        bundle.putInt("subInt", 6);
                        Intent intent = new Intent(MainActivity.this, ChooseSubActivity.class);
                        intent.putExtras(bundle);
                        startActivity(intent);
                    }
                })
                .setPositiveButton("Cancel", null)
                .show();
        return true;
    }

    @OnClick(R.id.sub8btn)
    public void sub8(){
        SharedPreferences prefs = getApplicationContext().getSharedPreferences("com.zachcole.redditgrid", Context.MODE_PRIVATE);
        startListView(prefs.getString("com.zachcole.redditgrid.sub7", ""));
    }

    @OnLongClick(R.id.sub8btn)
    public boolean sub8long(){
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(getApplicationContext().getSharedPreferences("com.zachcole.redditgrid", Context.MODE_PRIVATE).getString("com.zachcole.redditgrid.sub7", ""))
                .setMessage("Would you like to change the subreddit for slot #8?")
                .setNegativeButton("Change", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Bundle bundle = new Bundle();
                        bundle.putString("subnumber", Integer.toString(8));
                        bundle.putInt("subInt", 7);
                        Intent intent = new Intent(MainActivity.this, ChooseSubActivity.class);
                        intent.putExtras(bundle);
                        startActivity(intent);
                    }
                })
                .setPositiveButton("Cancel", null)
                .show();
        return true;
    }


    public void logOut(){
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Log Out")
                .setMessage("Are you sure you want to log out?")
                .setNegativeButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SharedPreferences prefs = getApplicationContext().getSharedPreferences("com.zachcole.redditgrid", MODE_PRIVATE);
                        SharedPreferences.Editor edit = prefs.edit();

                        //clears the token
                        edit.clear();
                        edit.apply();

                        Intent intent = new Intent(MainActivity.this, Login.class);
                        startActivity(intent);
                    }
                })
                .setPositiveButton("No", null)
                .show();
    }

    public void startListView(String sub){

        Bundle bundle = new Bundle();
        bundle.putString("suburl", sub);

        Intent intent = new Intent(this, FrontPage.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }
}
