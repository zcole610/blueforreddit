package com.zachcole.redditgrid;

/**
 * Created by Zach Cole on 7/19/2015.
 */
public class UserObject {

    String mUserName;

    public String getUserName() {
        return mUserName;
    }

    public void setUserName(String userName) {
        mUserName = userName;
    }
}
