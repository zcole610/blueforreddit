package com.zachcole.redditgrid;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

/**
 * Created by Zach Cole on 7/29/2015.
 */
public class CommentsAdapter extends BaseAdapter {

    private Context mContext;
    private CommentObject[] mComments;

    public CommentsAdapter(Context context, CommentObject[] comments){
        mContext = context;
        mComments = comments;

    }

    @Override
    public int getCount() {
        return mComments.length;
    }

    @Override
    public Object getItem(int position) {
        return mComments[position];
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        int padding = mComments[position].getPadding();
        int layer = mComments[position].getLayer();

        if(convertView == null){
            //brand new
            convertView = LayoutInflater.from(mContext).inflate(R.layout.comment_layout, null);
            holder = new ViewHolder();
            holder.commentBodyTV = (TextView)convertView.findViewById(R.id.commentBodyTV);
            holder.commentAuthor = (TextView)convertView.findViewById(R.id.commentAuthor);
            holder.commentScore = (TextView)convertView.findViewById(R.id.commentScore);
            holder.commentColorLL = (LinearLayout)convertView.findViewById(R.id.commentColorLL);
            holder.commentColor = (LinearLayout)convertView.findViewById(R.id.commentColor);

            holder.layer2 = (FrameLayout)convertView.findViewById(R.id.layer2);
            holder.layer3 = (FrameLayout)convertView.findViewById(R.id.layer3);
            holder.layer4 = (FrameLayout)convertView.findViewById(R.id.layer4);
            holder.layer5 = (FrameLayout)convertView.findViewById(R.id.layer5);
            holder.layer6 = (FrameLayout)convertView.findViewById(R.id.layer6);
            holder.layer7 = (FrameLayout)convertView.findViewById(R.id.layer7);
            holder.layer8 = (FrameLayout)convertView.findViewById(R.id.layer8);
            holder.layer9 = (FrameLayout)convertView.findViewById(R.id.layer9);
            holder.layer10 = (FrameLayout)convertView.findViewById(R.id.layer10);

            holder.upComment = (ImageButton)convertView.findViewById(R.id.upComment);
            holder.downComment = (ImageButton)convertView.findViewById(R.id.downComment);

            convertView.setTag(holder);
        }else{
            holder = (ViewHolder)convertView.getTag();
        }

        if(layer == 1){
            holder.layer2.setVisibility(View.INVISIBLE);
            holder.layer3.setVisibility(View.INVISIBLE);
            holder.layer4.setVisibility(View.INVISIBLE);
            holder.layer5.setVisibility(View.INVISIBLE);
            holder.layer6.setVisibility(View.INVISIBLE);
            holder.layer7.setVisibility(View.INVISIBLE);
            holder.layer8.setVisibility(View.INVISIBLE);
            holder.layer9.setVisibility(View.INVISIBLE);
            holder.layer10.setVisibility(View.INVISIBLE);
        } else if(layer == 2) {
            holder.layer2.setVisibility(View.VISIBLE);
            holder.layer3.setVisibility(View.INVISIBLE);
            holder.layer4.setVisibility(View.INVISIBLE);
            holder.layer5.setVisibility(View.INVISIBLE);
            holder.layer6.setVisibility(View.INVISIBLE);
            holder.layer7.setVisibility(View.INVISIBLE);
            holder.layer8.setVisibility(View.INVISIBLE);
            holder.layer9.setVisibility(View.INVISIBLE);
            holder.layer10.setVisibility(View.INVISIBLE);
        } else if(layer == 3){
            holder.layer2.setVisibility(View.VISIBLE);
            holder.layer3.setVisibility(View.VISIBLE);
            holder.layer4.setVisibility(View.INVISIBLE);
            holder.layer5.setVisibility(View.INVISIBLE);
            holder.layer6.setVisibility(View.INVISIBLE);
            holder.layer7.setVisibility(View.INVISIBLE);
            holder.layer8.setVisibility(View.INVISIBLE);
            holder.layer9.setVisibility(View.INVISIBLE);
            holder.layer10.setVisibility(View.INVISIBLE);
        } else if(layer == 4){
            holder.layer2.setVisibility(View.VISIBLE);
            holder.layer3.setVisibility(View.VISIBLE);
            holder.layer4.setVisibility(View.VISIBLE);
            holder.layer5.setVisibility(View.INVISIBLE);
            holder.layer6.setVisibility(View.INVISIBLE);
            holder.layer7.setVisibility(View.INVISIBLE);
            holder.layer8.setVisibility(View.INVISIBLE);
            holder.layer9.setVisibility(View.INVISIBLE);
            holder.layer10.setVisibility(View.INVISIBLE);
        } else if(layer == 5){
            holder.layer2.setVisibility(View.VISIBLE);
            holder.layer3.setVisibility(View.VISIBLE);
            holder.layer4.setVisibility(View.VISIBLE);
            holder.layer5.setVisibility(View.VISIBLE);
            holder.layer6.setVisibility(View.INVISIBLE);
            holder.layer7.setVisibility(View.INVISIBLE);
            holder.layer8.setVisibility(View.INVISIBLE);
            holder.layer9.setVisibility(View.INVISIBLE);
            holder.layer10.setVisibility(View.INVISIBLE);
        } else if(layer == 6){
            holder.layer2.setVisibility(View.VISIBLE);
            holder.layer3.setVisibility(View.VISIBLE);
            holder.layer4.setVisibility(View.VISIBLE);
            holder.layer5.setVisibility(View.VISIBLE);
            holder.layer6.setVisibility(View.VISIBLE);
            holder.layer7.setVisibility(View.INVISIBLE);
            holder.layer8.setVisibility(View.INVISIBLE);
            holder.layer9.setVisibility(View.INVISIBLE);
            holder.layer10.setVisibility(View.INVISIBLE);
        } else if(layer == 7){
            holder.layer2.setVisibility(View.VISIBLE);
            holder.layer3.setVisibility(View.VISIBLE);
            holder.layer4.setVisibility(View.VISIBLE);
            holder.layer5.setVisibility(View.VISIBLE);
            holder.layer6.setVisibility(View.VISIBLE);
            holder.layer7.setVisibility(View.VISIBLE);
            holder.layer8.setVisibility(View.INVISIBLE);
            holder.layer9.setVisibility(View.INVISIBLE);
            holder.layer10.setVisibility(View.INVISIBLE);
        } else if(layer == 8){
            holder.layer2.setVisibility(View.VISIBLE);
            holder.layer3.setVisibility(View.VISIBLE);
            holder.layer4.setVisibility(View.VISIBLE);
            holder.layer5.setVisibility(View.VISIBLE);
            holder.layer6.setVisibility(View.VISIBLE);
            holder.layer7.setVisibility(View.VISIBLE);
            holder.layer8.setVisibility(View.VISIBLE);
            holder.layer9.setVisibility(View.INVISIBLE);
            holder.layer10.setVisibility(View.INVISIBLE);
        } else if(layer == 9){
            holder.layer2.setVisibility(View.VISIBLE);
            holder.layer3.setVisibility(View.VISIBLE);
            holder.layer4.setVisibility(View.VISIBLE);
            holder.layer5.setVisibility(View.VISIBLE);
            holder.layer6.setVisibility(View.VISIBLE);
            holder.layer7.setVisibility(View.VISIBLE);
            holder.layer8.setVisibility(View.VISIBLE);
            holder.layer9.setVisibility(View.VISIBLE);
            holder.layer10.setVisibility(View.INVISIBLE);
        } else if(layer == 10){
            holder.layer2.setVisibility(View.VISIBLE);
            holder.layer3.setVisibility(View.VISIBLE);
            holder.layer4.setVisibility(View.VISIBLE);
            holder.layer5.setVisibility(View.VISIBLE);
            holder.layer6.setVisibility(View.VISIBLE);
            holder.layer7.setVisibility(View.VISIBLE);
            holder.layer8.setVisibility(View.VISIBLE);
            holder.layer9.setVisibility(View.VISIBLE);
            holder.layer10.setVisibility(View.VISIBLE);
        }

        holder.commentBodyTV.setText(mComments[position].getBody());
        //holder.commentBodyTV.setPadding(padding, 0, 0, 0);
        int dp = dpToPx(layer * 10);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.FILL_PARENT, FrameLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(dp, 0, 0, 0);
        holder.commentColor.setLayoutParams(params);

        holder.commentAuthor.setText(mComments[position].getAuthor());
        holder.commentScore.setText(Integer.toString(mComments[position].getScore()));

        if(mComments[position].getLikes() == 1) {
            //convertView.setBackgroundColor(Color.parseColor("#ff5700"));
            //holder.commentColorLL.setBackgroundColor(Color.parseColor("#ff5700"));
            holder.upComment.setImageResource(R.drawable.uporange);
            holder.commentScore.setTextColor(Color.parseColor("#ff5700"));
            holder.downComment.setImageResource(R.drawable.downwhite);
        }else if(mComments[position].getLikes() == -1){
//            convertView.setBackgroundColor(Color.parseColor("#0084d3"));
//            holder.commentColorLL.setBackgroundColor(Color.parseColor("#0084d3"));
            holder.upComment.setImageResource(R.drawable.upwhite);
            holder.commentScore.setTextColor(Color.parseColor("#0084d3"));
            holder.downComment.setImageResource(R.drawable.downblue);
        } else{
//            convertView.setBackgroundColor(Color.parseColor("#ff282828"));
//            holder.commentColorLL.setBackgroundColor(Color.parseColor("#ff282828"));
            holder.upComment.setImageResource(R.drawable.upwhite);
            holder.commentScore.setTextColor(Color.parseColor("#ffffff"));
            holder.downComment.setImageResource(R.drawable.downwhite);
        }


        return convertView;
    }

    private static class ViewHolder{
        TextView commentBodyTV;
        TextView commentAuthor;
        TextView commentScore;
        LinearLayout commentColorLL;
        LinearLayout commentColor;
        FrameLayout layer2;
        FrameLayout layer3;
        FrameLayout layer4;
        FrameLayout layer5;
        FrameLayout layer6;
        FrameLayout layer7;
        FrameLayout layer8;
        FrameLayout layer9;
        FrameLayout layer10;
        ImageButton upComment;
        ImageButton downComment;
    }

    public static int dpToPx(int dp)
    {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }
}
