package com.zachcole.redditgrid;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnClick;


public class FrontPage extends ListActivity {

    private static final String TAG = FrontPage.class.getSimpleName();
    RedditListItems[] mRedditArray = new RedditListItems[1002];
    private Activity activity = this;
    private Context context = this;
    int direction;
    int count = 0;
    int childLength = 0;
    Callback listCallback;
    String permalink = "";
    int mPos = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_front_page);

        Intent intent = getIntent();
//        Bundle extras = intent.getExtras();
//        if(extras != null){
//            if(extras.containsKey("position")){
//                count = extras.getInt("position", 0);
//            }
//        }
        String subUrl = intent.getExtras().getString("suburl");
        permalink = subUrl;
        Log.v(TAG, subUrl);



        listCallback = new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                Log.e(TAG, "onFailure callback");
                e.printStackTrace();
            }

            @Override
            public void onResponse(Response response) throws IOException {
                try{
                    String jsonBody = response.body().string();
                    Log.v(TAG, jsonBody);
                    if(response.isSuccessful()){
                        JSONObject reddit = new JSONObject(jsonBody);
                        JSONObject data = reddit.getJSONObject("data");
                        JSONArray children = data.getJSONArray("children");
                        childLength = children.length();

                        final RedditListItems[] items = new RedditListItems[children.length()];

                        for(int i = 0; i < children.length(); i++){
                            JSONObject data2 = children.getJSONObject(i);
                            JSONObject data3 = data2.getJSONObject("data");
                            RedditListItems item = new RedditListItems();
                            item.setThumbnail(data3.getString("thumbnail"));
                            item.setTitle(data3.getString("title"));
                            item.setScore(data3.getInt("score"));
                            item.setUrl(data3.getString("url"));
                            item.setOrder(count + i);
                            item.setCommentUrl(data3.getString("permalink"));
                            item.setSubreddit(data3.getString("subreddit"));
                            item.setId(data3.getString("id"));
                            item.setAuthor(data3.getString("author"));

                            if(data3.getString("likes") == "true"){
                                item.setLikes(1);
                            }else if(data3.getString("likes") == "false"){
                                item.setLikes(-1);
                            }else{
                                item.setLikes(0);
                            }

                            items[i] = item;
                            mRedditArray[getIndex()] = item;


                            Log.v(TAG,i + ": " + items[i].getThumbnail());

                        }




                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                RedditListItems[] current = new RedditListItems[getIndex()];
                                for(int i = 0; i < getIndex(); i++){
                                    current[i] = mRedditArray[i];
                                }
                                RedditListAdapter adapter = new RedditListAdapter(context, current);
                                setListAdapter(adapter);
                                getListView().setSelectionFromTop(count, 0);

                            }
                        });


                    }

                } catch (IOException e){
                    Log.e(TAG, "Exception caught: io");
                } catch (JSONException e){
                    Log.e(TAG, "Exception caught: json");
                }
            }
        };

        View footerView =  ((LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.tap_to_load, null, false);
        getListView().addFooterView(footerView);
        RedditHTTPClient.sharedInstance().getFrontPage(listCallback, subUrl);



//        for(int i = 0; i < mRedditArray.length; i++){
//            Log.v(TAG, "Reddit Array: " + mRedditArray[i].getTitle());
//        }

        //RedditListAdapter adapter = new RedditListAdapter(this, mRedditArray);
        //setListAdapter(adapter);


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_front_page, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

//    @Override
//    public void onResume() {
//        super.onResume();
//
//        RedditHTTPClient.sharedInstance().getFrontPage(listCallback, permalink);
//    }

    public void startWebView(View v){

        //This method handles clicking on the image in the ListView

        ListView list = getListView();
        int position = list.getPositionForView(v);

        Bundle bundle = new Bundle();
        String url = "";
        for(int i = 0; i < getIndex(); i++){
                if(mRedditArray[i].getOrder() == position){
                        url = mRedditArray[i].getUrl().toString();
                }
        }
        Log.v(TAG, "url: " + url);
        bundle.putString("urlstring", url);

        Intent intent = new Intent(this, WebViewActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    public void postDownVote(View v){

        String postId = "";

        final ListView list = getListView();
        final int position = list.getPositionForView(v);

        for(int i = 0; i < getIndex(); i++){
            if(mRedditArray[i].getOrder() == position){
                postId = mRedditArray[i].getId().toString();
                direction = mRedditArray[i].getLikes();
            }
        }

        Callback callback = new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {

            }

            @Override
            public void onResponse(Response response) throws IOException {
                Log.v(TAG, "Should post downvote");

                if(response.isSuccessful()){
                    Log.v(TAG, "Successful");
                }else{
                    Log.v(TAG, "Nope");
                    Log.v(TAG, response.message());
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        View v = getViewByPosition(position, list);
                        if(direction == 1 || direction == 0){
                            ((ImageButton) v.findViewById(R.id.upvoteImageBtn)).setImageResource(R.drawable.upwhite);
                            ((TextView) v.findViewById(R.id.scoreTextView)).setTextColor(Color.parseColor("#0084d3"));
                            ((ImageButton) v.findViewById(R.id.downvoteImageBtn)).setImageResource(R.drawable.downblue);
                        }else if(direction == -1){
                            ((ImageButton) v.findViewById(R.id.upvoteImageBtn)).setImageResource(R.drawable.upwhite);
                            ((TextView) v.findViewById(R.id.scoreTextView)).setTextColor(Color.parseColor("#ffffff"));
                            ((ImageButton) v.findViewById(R.id.downvoteImageBtn)).setImageResource(R.drawable.downwhite);
                        }
                    }
                });


            }
        };

        //Don't want to send unnecessary requests
        if(direction == 1 || direction == 0) {
            RedditHTTPClient.sharedInstance().postVote(-1, postId, callback);
        }else if (direction == -1){
            RedditHTTPClient.sharedInstance().postVote(0, postId, callback);
        }

        if(direction == 1 || direction == 0){
            mRedditArray[position].setLikes(-1);
        }else if (direction == -1){
            mRedditArray[position].setLikes(0);
        }
    }

    public void postUpVote(View v){

        String postId = "";

        final ListView list = getListView();
        final int position = list.getPositionForView(v);

        for(int i = 0; i < getIndex(); i++){
            if(mRedditArray[i].getOrder() == position){
                postId = mRedditArray[i].getId();
                direction = mRedditArray[i].getLikes();
            }
        }

        Callback callback = new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {

            }

            @Override
            public void onResponse(Response response) throws IOException {
                Log.v(TAG, "Should post downvote");

                if(response.isSuccessful()){
                    Log.v(TAG, "Successful");
                    Log.v(TAG, response.body().string());
                }else{
                    Log.v(TAG, "Nope");
                    Log.v(TAG, response.message());
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        View v = getViewByPosition(position, list);
                        if(direction == 0 || direction == -1){
                            ((ImageButton) v.findViewById(R.id.upvoteImageBtn)).setImageResource(R.drawable.uporange);
                            ((TextView) v.findViewById(R.id.scoreTextView)).setTextColor(Color.parseColor("#ff5700"));
                            ((ImageButton) v.findViewById(R.id.downvoteImageBtn)).setImageResource(R.drawable.downwhite);
                        }else if(direction == 1){
                            ((ImageButton) v.findViewById(R.id.upvoteImageBtn)).setImageResource(R.drawable.upwhite);
                            ((TextView) v.findViewById(R.id.scoreTextView)).setTextColor(Color.parseColor("#ffffff"));
                            ((ImageButton) v.findViewById(R.id.downvoteImageBtn)).setImageResource(R.drawable.downwhite);
                        }
                    }
                });


            }
        };

        //Don't want to send unnecessary request
        if(direction == -1 || direction == 0) {
            RedditHTTPClient.sharedInstance().postVote(1, postId, callback);
        }else if (direction == 1){
            RedditHTTPClient.sharedInstance().postVote(0, postId, callback);
        }

        if(direction == -1 || direction == 0) {
            mRedditArray[position].setLikes(1);
        }else if (direction == 1){
            mRedditArray[position].setLikes(0);
        }
    }

    //Method that helps handle the return of proper listview position for voting code
    public View getViewByPosition(int pos, ListView listView) {
        final int firstListItemPosition = listView.getFirstVisiblePosition();
        final int lastListItemPosition = firstListItemPosition + listView.getChildCount() - 1;

        if (pos < firstListItemPosition || pos > lastListItemPosition ) {
            return listView.getAdapter().getView(pos, null, listView);
        } else {
            final int childIndex = pos - firstListItemPosition;
            return listView.getChildAt(childIndex);
        }
    }

    public void startComments(View v){

        ListView list = getListView();
        int position = list.getPositionForView(v);

        Bundle bundle = new Bundle();
        String permalink = "";

        for(int i = 0; i < getIndex(); i++){
            if(mRedditArray[i].getOrder() == position){
                permalink = mRedditArray[i].getCommentUrl();
            }
        }
        Log.v(TAG, "url: " + permalink);
        bundle.putString("urlstring", permalink);
        bundle.putInt("position", position);

        Intent intent = new Intent(this, Comments.class);
        intent.putExtras(bundle);
        startActivity(intent);

    }

    public void addItems(View v){
        String id = "t3_" + mRedditArray[getIndex() - 1].getId();
        int limit = 25;

        RedditHTTPClient.sharedInstance().getFrontPageMore(listCallback, id, count, limit, permalink);
        count += childLength;

        for(int i = 0; i < getIndex(); i++){
            Log.v(TAG, mRedditArray[i].getTitle());
        }


//        runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                getListView().getAdapter().notify();
//            }
//        });

    }

    public int getIndex(){
        int index = 0;
        for(int i = 0; i < mRedditArray.length + 2; i++){
            if(mRedditArray[i] == null){
                index = i;
                break;
            }
        }
        return index;

    }

}
