package com.zachcole.redditgrid;

/**
 * Created by Zach Cole on 7/28/2015.
 */
public class CommentObject {
    //Data for the listing
    private String mTitle;
    private String mThumbnail;
    private int mScore;
    private String mSubreddit;
    private int mLikes;
    private String mUrl;
    private String mAuthor;
    private String mBody;
    private int mPadding;
    private String mId;
    private int mLayer;


    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getThumbnail() {
        return mThumbnail;
    }

    public void setThumbnail(String thumbnail) {
        if(thumbnail.isEmpty()){
            mThumbnail = "default";
        }else{
            mThumbnail = thumbnail;
        }
    }

    public int getScore() {
        return mScore;
    }

    public void setScore(int score) {
        mScore = score;
    }

    public String getSubreddit() {
        return mSubreddit;
    }

    public void setSubreddit(String subreddit) {
        mSubreddit = subreddit;
    }

    public int getLikes() {
        return mLikes;
    }

    public void setLikes(int likes) {
        mLikes = likes;
    }

    public String getAuthor() {
        return mAuthor;
    }

    public void setAuthor(String author) {
        mAuthor = author;
    }

    public String getBody() {
        return mBody;
    }

    public void setBody(String body) {
        mBody = body;
    }

    public int getPadding() {
        return mPadding;
    }

    public void setPadding(int padding) {
        mPadding = padding;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public int getLayer() {
        return mLayer;
    }

    public void setLayer(int layer) {
        mLayer = layer;
    }
}
