package com.zachcole.redditgrid;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class ReplyActivity extends ActionBarActivity {

    private static final String TAG = ReplyActivity.class.getSimpleName();
    Callback callback;
    String mBody = "";
    String mId = "";
    String mPermalink = "";
    int mPos = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reply);

        ButterKnife.inject(this);

        Intent intent = getIntent();
        String author = intent.getExtras().getString("author");
        String body = intent.getExtras().getString("body");
        int score = intent.getExtras().getInt("score");
        String id = intent.getExtras().getString("id");
        String permalink = intent.getExtras().getString("permalink");
        mPermalink = permalink;
        mId = id;
        mPos = intent.getExtras().getInt("position");

        TextView replyBody = (TextView)findViewById(R.id.replyBody);
        TextView replyAuthor = (TextView)findViewById(R.id.replyAuthor);
        replyBody.setText(body);
        replyAuthor.setText(author);



        callback = new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                Log.e(TAG, "onFailure:");
                e.printStackTrace();
            }

            @Override
            public void onResponse(Response response) throws IOException {
                String jsonBody = response.body().string();
                Log.v(TAG, jsonBody);

                //Toast.makeText(getApplicationContext(), "Successfully posted!", Toast.LENGTH_SHORT).show();

                StartComments();
            }
        };
    }

    @OnClick (R.id.submitComment)
    public void postComment(){
        EditText replyText = (EditText)findViewById(R.id.replyText);
        String replyPost = replyText.getText().toString();
        mBody = replyPost;

        RedditHTTPClient.sharedInstance().postComment(callback, mBody, mId);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_reply, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void StartComments(){

        Bundle bundle = new Bundle();

        bundle.putString("urlstring", mPermalink);
        bundle.putInt("position", mPos);

        Intent intent = new Intent(this, Comments.class);
        intent.putExtras(bundle);
        startActivity(intent);

        this.finish();
    }
}
