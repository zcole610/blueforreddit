package com.zachcole.redditgrid;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import butterknife.ButterKnife;
import butterknife.OnClick;


public class ChooseSubActivity extends ActionBarActivity {

    private static final String TAG = ChooseSubActivity.class.getSimpleName();
    String[] subs = new String[50];

    int pos = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_sub);

        Intent intent = getIntent();
        final String subNumber = intent.getExtras().getString("subnumber");
        final int subInt = intent.getExtras().getInt("subInt");



        ListView searchList = (ListView)findViewById(R.id.search_list);
        searchList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(getBaseContext(), subs[position], Toast.LENGTH_LONG).show();
                pos = position;

                if(!subs[position].equals("No results found. Try again.")) {
                    new AlertDialog.Builder(ChooseSubActivity.this)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setTitle(subs[position])
                            .setMessage("Would you like " + subs[position] + " to be subreddit #" + subNumber + "?")
                            .setNegativeButton("Confirm", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    SharedPreferences prefs = getApplicationContext().getSharedPreferences("com.zachcole.redditgrid", Context.MODE_PRIVATE);
                                    SharedPreferences.Editor edit = prefs.edit();
                                    edit.putString("com.zachcole.redditgrid.sub" + Integer.toString(subInt), subs[pos]);
                                    edit.apply();

                                    Intent intent = new Intent(ChooseSubActivity.this, MainActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                            })
                            .setPositiveButton("Cancel", null)
                            .show();
                }
            }
        });

        ButterKnife.inject(this);
    }

    @OnClick (R.id.search_button)
    public void search(){
        hideKeyboard();
        EditText searchText = (EditText)findViewById(R.id.search_text);
        String searchString = searchText.getText().toString();

        Callback callback = new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {

            }

            @Override
            public void onResponse(Response response) throws IOException {
                if(response.isSuccessful()){
                    try{
                        String jsonBody = response.body().string();
                        Log.v(TAG, jsonBody);
                        JSONObject json = new JSONObject(jsonBody);
                        JSONObject data = json.getJSONObject("data");
                        JSONArray children = data.getJSONArray("children");
                        if(children.length() == 0){
                            subs[0] = "No results found. Try again.";
                        }else {
                            for (int i = 0; i < children.length(); i++) {
                                JSONObject child = children.getJSONObject(i);
                                JSONObject data2 = child.getJSONObject("data");
                                subs[i] = data2.getString("url");
                                Log.v(TAG, Integer.toString(i) + ": " + subs[i]);
                            }
                        }

                        final String[] subsList = new String[getIndex()];
                        for(int i = 0; i < getIndex(); i++){
                            subsList[i] = subs[i];
                        }

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                ListView searchList = (ListView)findViewById(R.id.search_list);
                                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_list_item_1, android.R.id.text1, subsList);
                                searchList.setAdapter(adapter);
                            }
                        });
                    }catch (JSONException e){
                        Log.e(TAG, "JSON Exception:");
                        e.printStackTrace();
                    }
                }
            }
        };

        RedditHTTPClient.sharedInstance().searchSubs(callback, searchString);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_choose_sub, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public int getIndex(){
        int index = 0;
        for(int i = 0; i < subs.length + 2; i++){
            if(subs[i] == null){
                index = i;
                break;
            }
        }
        return index;

    }

    private void hideKeyboard() {
        // Check if no view has focus:
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

}
